# Certificate for Pi Star

Diese Dokumentation enthält, ergänzend zum Video auf Youtube, die Copy'n Paste fähigen Befehle die notwendig sind ein Schlüsselpaar für die https-Verschlüsselung zu generieren. Die Beispiele orientieren sich am nginx für Pi-Star, gelten analog aber für jeden Serverprozess der TLS verschlüsselt.

## openssl
Entweder (1) per "_apt_" openssl installieren oder (unter 2 beschrieben die bessere Variante) openssl selbst compilieren damit die aktuelle Version zum Einsatz kommt.

### Variante 1: apt install

```
root@pi-star(ro):~# rpi-rw
root@pi-star(rw):~# apt install openssl
```



### Variante 2: Sourcecode compilieren

Von [https://www.openssl.org](https://www.openssl.org/source/) die aktuellste Version des 1.1-Zweigs direkt auf den Raspeberry Pi runter laden, auspacken, durch den Compiler schicken und installieren. Zuerst, da die SD-Karte in der Regel _Read Only_ gemountet ist, die Rootpartition _Read Write_ mounten. Stand Mai 2021 also:

```
pi-star@pi-star(ro):~$ rpi-rw 
pi-star@pi-star(rw):~$ mkdir src
pi-star@pi-star(rw):~$ cd src
pi-star@pi-star(rw):src$ wget https://www.openssl.org/source/openssl-1.1.1k.tar.gz
converted 'https://www.openssl.org/source/openssl-1.1.1k.tar.gz' (ANSI_X3.4-1968) -> 'https://www.openssl.org/source/openssl-1.1.1k.tar.gz' (UTF-8)
--2021-05-14 08:05:07--  https://www.openssl.org/source/openssl-1.1.1k.tar.gz
Resolving www.openssl.org (www.openssl.org)... 23.54.97.163, 2a02:26f0:1500:197::c1e, 2a02:26f0:1500:194::c1e
Connecting to www.openssl.org (www.openssl.org)|23.54.97.163|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9823400 (9.4M) [application/x-gzip]
Saving to: 'openssl-1.1.1k.tar.gz'

openssl-1.1.1k.tar.gz                                      100%[=========================================================================================================================================>]   9.37M  4.27MB/s   in 2.2s   

2021-05-14 08:05:10 (4.27 MB/s) - 'openssl-1.1.1k.tar.gz' saved [9823400/9823400]

pi-star@pi-star(rw):src$ tar xvf openssl-1.1.1k.tar.gz
openssl-1.1.1k/
openssl-1.1.1k/ACKNOWLEDGEMENTS
openssl-1.1.1k/AUTHORS
openssl-1.1.1k/CHANGES
openssl-1.1.1k/CONTRIBUTING
.
.
openssl-1.1.1k/util/shlib_wrap.sh.in
openssl-1.1.1k/util/su-filter.pl
openssl-1.1.1k/util/unlocal_shlib.com.in
pi-star@pi-star(rw):src$ cd openssl-1.1.1k/
pi-star@pi-star(rw):openssl-1.1.1k$ ./config
Operating system: armv7l-whatever-linux2
Configuring OpenSSL version 1.1.1k (0x101010bfL) for linux-armv4
Using os-specific seed configuration
Creating configdata.pm
Creating Makefile

**********************************************************************
***                                                                ***
***   OpenSSL has been successfully configured                     ***
***                                                                ***
***   If you encounter a problem while building, please open an    ***
***   issue on GitHub <https://github.com/openssl/openssl/issues>  ***
***   and include the output from the following command:           ***
***                                                                ***
***       perl configdata.pm --dump                                ***
***                                                                ***
***   (If you are new to OpenSSL, you might want to consult the    ***
***   'Troubleshooting' section in the INSTALL file first)         ***
***                                                                ***
**********************************************************************


pi-star@pi-star(rw):openssl-1.1.1k$ make
/usr/bin/perl "-I." -Mconfigdata "util/dofile.pl" \
    "-oMakefile" include/crypto/bn_conf.h.in > include/crypto/bn_conf.h
/usr/bin/perl "-I." -Mconfigdata "util/dofile.pl" \
    "-oMakefile" include/crypto/dso_conf.h.in > include/crypto/dso_conf.h
.
.
.
pi-star@pi-star(rw):openssl-1.1.1k$ sudo make install
```

Das läuft auf meinem RasPi rund 50 Minuten. Danach muss die Datei _/etc/ld.so.conf_ noch um eine Zeile erweitert werden.


```
pi-star@pi-star(rw):~$ cat /etc/ld.so.conf
include /etc/ld.so.conf.d/*.conf
include /usr/local/lib
```

Nun könnte (!) _openssl_ konfiguriert werden um dann Schlüsselpaare zu generieren. Dieses Vorgehen empfehle ich ausdrücklich nicht. Für unsere Anwendung ist das zu aufwändig. Wer es trotzdem so versuchen möchte findet 
[hier](https://jamielinux.com/docs/openssl-certificate-authority/introduction.html) eine gute und ausführliche Anleitung. 


## easyrsa
Die Handhabung von Zertifikaten (genauer: Schlüsselpaaren. Eines nennen wir _geheimer Schlüssel_ den anderen Schlüssel nennen wir vereinfachend _Zertifikat_ obwohl er in Wirklichkeit aus dem öffentlichen Schlüssel und (!) dem Zertifikat besteht) wird durch _easyrsa_ viel einfacher. Entwickelt wurde diese Scriptsammlung für die Schlüsselverwaltung von OpenVPN. Sie kann aber auch für andere Zwecke verwendet werden.

Zuerst wird _easyrsa_ runtergeladen und ausgepackt. Derzeit ist die Version 3 aktuell. Die findet sich auf auf den Projektservern von  [OpenVPNtext](ttps://github.com/OpenVPN/easy-rsa/releases)

### Installation

```
pi-star@pi-star(rw):~$ cd src/
pi-star@pi-star(rw):src$ wget https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz
converted 'https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz' (ANSI_X3.4-1968) -> 'https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz' (UTF-8)
--2021-05-14 08:54:21--  https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz
.
.
pi-star@pi-star(rw):src$ tar xvf EasyRSA-3.0.8.tgz 
EasyRSA-3.0.8/
.
.
pi-star@pi-star(rw):src$ cd EasyRSA-3.0.8/
```
Eine gute Dukomentation zu _easyrsa_ findet sich im [Easy-RSA v3 OpenVPN Howto](https://community.openvpn.net/openvpn/wiki/EasyRSA3-OpenVPN-Howto?__cf_chl_jschl_tk__=e239cec93a08f1de8c0f682b02cae418f4e44141-1620672825-0-Ac6yFPFqngTHCLy1JVoMnpvTcflb62l9pBG1iakvKPk7fE6zTQJA3_Lgif1ekXrlduIShJZTKGhiWuRFQC6X8KayryL3seyzzlYLLQ6jxdjFQtiP-EmX0bJmhWu0faEgSZE1Pm2KoxTp67FDqa54uN6282915zV3SFqKhWGOwu3Gp-vKaa2Oms5V-ENJC56iBI_IwruO1bPz_ruWbrrR3L5Seg0jTIERvTBdg-ZrIyFEcY9zqoOJTbadw9PEyXpRaq9KOrqvk8zhL116t56WYnU1YtMp18WawkhAC9Okt4J9bKthiLm-s2ambMFzFrrTgDL5Ei236K9JuaQ2j6U11KQigEASY6KQE-1SqlLHIKV40ysrjTgTt8U18yNN3sjxWzF5qYJ4y6rPixRi6hwwxJSRqci9P56nbjEhEdm1BIOXuIz5UQoRQ_aEIjXtg8wcpQ)


Jetzt wird die CA initialisiert und ein Schlüsselpaar erzeugt. 

### CA generieren
```
pi-star@pi-star(rw):EasyRSA-3.0.8$ ./easyrsa init-pki
init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /home/pi-star/src/EasyRSA-3.0.8/pki

pi-star@pi-star(rw):EasyRSA-3.0.8$ ./easyrsa build-ca
Using SSL: openssl OpenSSL 1.1.1k  25 Mar 2021

Enter New CA Key Passphrase: ********
Re-Enter New CA Key Passphrase: ********
Generating RSA private key, 2048 bit long modulus (2 primes)
..............................................................................................................+++++
..+++++
e is 65537 (0x010001)
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:DD0ULs Root CA

CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/home/pi-star/src/EasyRSA-3.0.8/pki/ca.crt
```

Die hier gesetzte Passphrase darf nicht verloren gehen, sonst ist die CA perdu. Eine gute Möglichkeit solche Dinge zu verwalten bietet [keepass](https://keepass.info/). Denn wer kann sich eine nur selten genutzte Passphrase schon 10 Jahre oder länger merken? Eine Alternative für die Passphraseproblematik wäre es, erst gar keine Passphrase für die CA zu setzen. Das in gewisser Weise ein Sicherheitsproblem, im kleinen Rahmen eines einzigen Webservers für Hobbyzwecke aber wohl akzeptabel. Soll keine Passwphrase vergeben werden lautet der Befehl zur Generierung der CA:

```
pi-star@pi-star(rw):EasyRSA-3.0.8$ ./easyrsa build-ca nopass

```
Fällt die Entscheidung keine Passphrase nutzen zu wollen erst später, dann sie mit diesem Befehl entfernt werden

```
pi-star@pi-star(ro):EasyRSA-3.0.8$  openssl rsa -in /home/pi-star/src/EasyRSA-3.0.8/pki/private/ca.key -out /tmp/newPrivateKey.pem
pi-star@pi-star(ro):EasyRSA-3.0.8$  mv /tmp/newPrivateKey.pem /home/pi-star/src/EasyRSA-3.0.8/pki/private/ca.key
```

### Key und Zertifikat für den nginx generieren
Jetzt wird das Schlüsselpaar generiert. Da der Webserver bei mir sowohl unter den Namen pi-star.fritz.box und pi-star.local als auch unter der IP-Adresse 192.168.5.83 erreichbar ist werden diese Werte als "Subject Alternative Name" (SAN) angegeben.
```
pi-star@pi-star(rw):EasyRSA-3.0.8$ ./easyrsa --subject-alt-name="DNS:pi-star.local,DNS:pi-star.fritz.box,DNS:192.168.5.83"  build-server-full pi-star.fritz.box nopass
Using SSL: openssl OpenSSL 1.1.1k  25 Mar 2021
Generating a RSA private key
........................+++++
...........................................................+++++
writing new private key to '/home/pi-star/src/EasyRSA-3.0.8/pki/easy-rsa-11409.6wEvyj/tmp.bNt0Yw'
-----
Using configuration from /home/pi-star/src/EasyRSA-3.0.8/pki/easy-rsa-11409.6wEvyj/tmp.cXMj3n
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'pi-star.fritz.box'
Certificate is to be certified until Aug 17 10:15:56 2023 GMT (825 days)

Write out database with 1 new entries
Data Base Updated
```

Falls für die RootCA eine Passphrase vergeben wurde ist diese anzugeben. Sonst eben, wie im Beispiel hier, nicht. Der Schalter "nopass" bei der Generierung ist wichtig. Er sorgt dafür, dass der private Key für den Webserver nicht passphrasegeschützt ist. Wer sollte das beim Start des nginx auch eingeben?

## nginx konfigurieren

Jetzt den nginx konfigurieren. Zertifikat und Schlüssel werden wahlweise in die bereits existierenden Verzeichnisse _/etc/ssl/certs/_ und _/etc/ssl/private/_ kopiert oder in ein zu erstellendes _/etc/nginx/certs_. 

```
root@pi-star(rw):~# mkdir /etc/nginx/certs
root@pi-star(rw):~# cp -p /home/pi-star/src/EasyRSA-3.0.8/pki/issued/pi-star.fritz.box.crt /etc/nginx/certs/pi-star.fritz.box-cert.pem 
root@pi-star(rw):~# cp -p /home/pi-star/src/EasyRSA-3.0.8/pki/private/pi-star.fritz.box.key /etc/nginx/certs/pi-star.fritz.box-privkey.pem 
```

Die Konfigurationsdatei des nginx wird wie folgt angepasst:
1. https mit den gerade kopierten Dateien auf Port 443
2. Der http-Port 80 macht nur noch einen Redirect (301) auf den https Port
Eine beispielhafte nginx.conf liegt hier.


Nach einem Neustart des nginx läuft der Webserver auf Port 80 und auf 443. Eine beispielhafte /etc/nginx/sites-available/pi-star liegt hier und sollte in den meisten Fällen 1:1 übernommen werden können.

```
root@pi-star(rw):~# netstat -tulpen | grep nginx
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      0          12511       992/nginx -g daemon
tcp        0      0 0.0.0.0:443             0.0.0.0:*               LISTEN      0          12510       992/nginx -g daemon
```




## firewall konfigurieren
Den Zugriff vom Browser aus blokiert jedoch noch die Firewall.  Zwar sind ausgehende Verbindungen auf Port 443 erlaubt (alleine schon um mit _apt update_ neue Pakete holen zu können), eingehend ist Port 443 aber zu. Deswegen wird folgende Zeile in das  custom firewall script _/root/ipv4.fw_ eingefügt:

```
iptables -A INPUT -p tcp --dport 443 -j ACCEPT #                    HTTPS Allow for ngnix 

```

dieses Textsnipsel wird jedes mal wenn die Firewall startet (/usr/local/sbin/pistar-firewall) eingebunden. Um den Port 443 jetzt zu öffnen, die neue Konfiguration also zu aktivieren, muss entweder das Script ausgeführt werden der der RasPi rebootet werden. Details dazu auf den Seiten des [https://wiki.pistar.uk](https://wiki.pistar.uk/Adding_custom_firewall_rules_to_Pi-Star)

## redirect im nginx konfigurieren
Jetzt bleibt noch ein Problem: Mit der derzeitigen Konfiguration ist der Webserver verschlüsselt auf Port 443 erreichbar, aber auch noch unverschlüsselt auf Port 80, dem Standardport für http. Deswegen wird die Konfigurationsdatei nun so verändert, dass bei einem Zugriff auf Port 80 nur ein Redirect auf Port 443 zum Browser geschickt wird. Und schon sind wir auf der sicheren Seite. Die Konfigurationsdatei _/etc/nginx/sites-available/pi-star_ sieht so aus:
```
server {
        #listen 80 default_server;
        listen 443 ssl;
        root /var/www/dashboard;
        server_name             pi-star.fritz.box;
        ssl_certificate         certs/pi-star.fritz.box-cert.pem;
        ssl_certificate_key     certs/pi-star.fritz.box-privkey.pem;
        location ^~ /admin {
                try_files $uri $uri/ =404;
                auth_basic "Restricted";
                auth_basic_user_file /var/www/.htpasswd;
                client_max_body_size 512K;

        include             /etc/nginx/default.d/php.conf;
        }

        location ~ /\.git {
                deny all;
        }

        # Load the defaults
        include             /etc/nginx/default.d/*.conf;
}
server {
       listen 80 default_server;
       return 301 https://$host$request_uri;
}
```


## Zertifikat der CA im Browser importieren

Jetzt endlich ist der Webserver für Pi-Star mit https abgesichert. Allerding gibt der Browser noch immer eine Warnung aus: Die CA, die das Zertifikat des Webservers gesignt hat sei unbekannt. Das stimmt, denn die Entwickler der Browser packen zwar die Zertifikate der grossen CAs wie Comodo und RapidSSL in den Zertifikatsspeicher, aber unsere kleine CA auf Basis von _easyrsa_ kennen sie natürlich nicht. Deswegen müssen wir das selbst erledigen. Im Firefox als unter Preferenzes | Privacy & Security | Certificats | View Certificats ... | Authorities | Import ... die Datei _/home/pi-star/src/EasyRSA-3.0.8/pki/ca.crt_ hochladen und im anschliessenden Dialog "für Server" bestätigen. Dieser [Screenshot](https://gitlab.com/dd0ul/certificate_for_pi-star/-/blob/master/ImportCA.png) hilft die einzelnen Schritte nachvollziehen zu können.

## /etc/motd anpassen

Bei jedem Login per ssh wird der Inhalt der Datei _/etc/motd_ angezeigt.

```
╔═══════════════════════════════════════════════════════════════════════╗
║                                                                       ║
║           ██████╗ ██╗      ███████╗████████╗ █████╗ ██████╗           ║
║           ██╔══██╗██║      ██╔════╝╚══██╔══╝██╔══██╗██╔══██╗          ║
║           ██████╔╝██║█████╗███████╗   ██║   ███████║██████╔╝          ║
║           ██╔═══╝ ██║╚════╝╚════██║   ██║   ██╔══██║██╔══██╗          ║
║           ██║     ██║      ███████║   ██║   ██║  ██║██║  ██║          ║
║           ╚═╝     ╚═╝      ╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝          ║
║                                                                       ║
╚═══════════════════════════════════════════════════════════════v3.4.13═╝
From your Windows Computer:
Pi-Star Dashboard:      https://pi-star/

From your Apple iPhone, iPad, Macbook, iMac etc.
Pi-Star Dashboard:      https://pi-star.local/

Pi-Star's disk is read-only by default, enable read-write with "rpi-rw".
Pi-Star built by Andy Taylor (MW0MWZ), pi-star tools all start "pistar-".
```

Dass dort noch http statt https steht ist eine Kleinigkeit, stört aber das runde Bild. Deswegen wird mit einem Editor (besser den nano verwenden, der vi hat Probleme mit dem Steuersequenzen in der Datei) und die fehlenden _s_ ergänzen.
```
pi-star@pi-star(rw):~$ sudo nono /etc/motd 
```

## Die Zertifikatsverwaltung bei mir 

Ich selbst generiere meine Zertifikate aufwändiger. Das ist aber nichts, das "mal schnell" in einigen Minuten oder Stunden installiert werden kann. Mein Setup entstand über Jahre und kann eben viel mehr für die Verschlüsselung des Webservers für den MMDVM zu sorgen. Das passiert vollständig automatisch, ohne dass ich mich um etwas kümmern müsste. Der Vollständigkeit halber sei mein Setup hier kurz vorgestellt.

Statt _openssl_ nutze ich [_Hashicorp Vault_](https://www.vaultproject.io/ ) um Zertifikate zu verwalten. Genau genommen ist HCV eine Secretverwaltung, die eben auch CA ist und Zertifikate generieren kann. Der Prozess läuft ständig auf unserem "Wohungsserver", einem älteren Notebook. Auf dem läuft auch der Printserver (_CUPS_), er kümmert sich um das Backup [_r5backup_](https://sourceforge.net/projects/r5backup/) und einiges mehr. Damit die Zertifikate bei Bedarf neu generiert und eingespielt werden wird auf dem pi-star täglich um 09:15 Uhr via _cron_ [_vaultbot_](https://gitlab.com/msvechla/vaultbot )aufgerufen. Der prüft die "Restlaufzeit" des vorhandenen Zertifikats und erneuert es rechtzeitig bevor es ausläuft und startet dann den _nginx_ neu.

```
10 9    * * *   root    mount -o remount,rw /
# cretate certificate for 45 days, but renew it 48h befor it expires. Check every day
# token is valid for 30day but is renewed
# nginx is restartet when the certificate was renewed

15 9    * * *   root /usr/local/bin/vaultbot --vault_addr=https://hermes.fritz.box:8200 --vault_token=s.*************** --pki_common_name=pi-star.fritz.box --pki_ttl=768h --pki_renew_time=48h  --pki_mount=pki_int --pki_role_name=fritz-dot-box --pki_cert_path=/etc/nginx/certs/pi-star.fritz.box-cert.pem --pki_alt_names=pi-star.local --pki_privkey_path=/etc/nginx/certs/pi-star.fritz.box-privkey.pem -logfile=/var/log/vaultbot.txt --renew_hook="systemctl restart nginx" --vault_renew_token
```

Andere Zertifikate (die für den Printserver beispielsweise) werden mit Hilfe von [Jenkins](https://www.jenkins.io/) generiert und verteilt. Da mein eingesetztes Intermidiate Zertifikat eine Laufzeit von "nur" 5 Jahren hat gibt es auch einen Jenkins-Job, der dieses Zertifikat nach 4 Jahren erneuert und auf die einzelnen Rechner hier kopiert. Unter Linux also nach _/etc/ssl/certs_. Da die Browser in der Regel eine eigene Zertifikatsverwaltung mitbringen, wird das neue Zertifikat mit Hilfe von [certutil](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/tools/NSS_Tools_certutil) direkt in den Zertifikatsspeicher des Firefox gebracht. 
